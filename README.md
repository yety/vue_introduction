# Vorstellung VueJS 

## Installation VueJS
* Node installieren in PATH
* .npmrc für die Hütte:
  * Pfad: ``c:\Users\<username>\.npmrc``
  * Inhalt:
  ```
  strict-ssl=false 
  ssl-strict=false 
  proxy=XXX
  https-proxy=XXX
  ```
* npm install --global @vue/cli
* vue create my-project-name
* Auswahl
  * Choose Version 2.0 (3 brandaktuell und noch nicht alle Module fertig)
  * Babel
  * TypeScript
  * Router
  * Vuex
  * Linter / Formatter
* Vue Version 2.x
* Rest der Fragen yes/default 
* cd my-project-name
* npm run serve
## IDE erklären
### VS Code
* VS Code Vetur installieren
* Open folder 
* indent using spaces 2
### Jetbrains WebStorm
* Projekt öffnen 
* kurz zeigen
## Einführung Vue
* App.vue erklären
  * template / script / style
* kurz erklären, was TypeScript ist
* router erklären
* neue Komponente bauen 
* einfaches Binding erstellen (message)
* input mit Binding auf message erstellen
* Reset-button mit @click methode erstellen
* Computed Property einbauen (Zwei Zahlen addieren)
* Vue Plugin für Chrome vorstellen

## TodoList
* TodoList und TodoItem erstellen
* erst TodoItem aus data holen
* Darstellung von TodoItem in eigener Komponente kapseln
* axios installieren: ``npm install axios``
* TodoItems mit Axios holen
** network tab in Debugger zeigen
** json -> TodoItem
* TodoItem in eigener Komponente darstellen
* Laden in mounted()-Methode
* Lifecylcle Hooks:
![vue lifecycle](https://vuejs.org/images/lifecycle.png)
* Alternative: Bootstrap table


## Bootstrap-vue hinzufügen
* Installation Bootstrap-vue: `npm install bootstrap-vue bootstrap`
* kurz package.json erklären
* Hinzufügen zur Applikation in main.ts:
```javascript
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
```

* Grid erklären
  * container in App.vue
  * row/col in Test für die einzelnen Eingabefelder

## Props und Events
* MessageEditor Komponente erstellen und erklären

## GET request mit Axios in Component


## Vuex
* Vuex kurz erklären
* Installation: ``npm install --save vuex vuex-class``