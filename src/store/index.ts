import { StoreState } from '@/types/states/StoreState'
import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'

Vue.use(Vuex)

const store: StoreOptions<StoreState> = {
  state: {
    value: ""       
  },
  mutations: {
    setValue(state: StoreState, value: string) {
      state.value = value;
    },
  },
  getters: {
    getValue(state: StoreState) {
      return state.value;
    }
  },
  actions: {
    async fetchValue() {
      // const response = await fetch('https://jsonplaceholder.typicode.com/todos/1');
      // const json = JSON.parse(await response.json());
      // const title = json.title; 
      // console.log(title);
      // dispatch("setValue", title);
    }
  },
  modules: {},
};

const vuexStore = new Vuex.Store<StoreState>(store);
export default vuexStore;
